"""Django modelos utilidades"""

#Django
from django.db import models

class CRideModel(models.Model):
    """Comparte Ride base model.

        acts es una abstraccion base  de varios modeles en el proyecto
    """
    created = models.DateTimeField('created at', auto_now_add=True, help_text="Es el tiempo que el objeto es creado")
    modified = models.DateTimeField('modified at', auto_now_add=True, help_text="Es el tiempo que el objeto es actualizado")


    class Meta:
        """Meta opciones"""
        abstract = True
        get_latest_by = 'created'
        ordering = ['-created', '-modified']

