""" Membership model . (membresias)"""
# Django
from django.db import models
# Utilities
from cride.utils.models import CRideModel

class Membership(CRideModel):
    """ membership es una tabla intermedia  para las relaciones de un usuario y un circulo para meter datos adicionales"""

    user = models.ForeignKey('users.User', on_delete=models.CASCADE)
    profile = models.ForeignKey('users.Profile', on_delete=models.CASCADE)
    circle = models.ForeignKey('circles.Circle', on_delete=models.CASCADE)

    id_admin = models.BooleanField(
        'circle admin',
        default=False,
        help_text = "Circulos administradores pueden actualizar o borrar datos de los miembros"
    )
    # Invitaciones
    used_invitations = models.PositiveSmallIntegerField(default=0)
    remaining_invitations = models.PositiveSmallIntegerField(default=0)
    invited_by = models.ForeignKey('users.User', null=True, on_delete=models.SET_NULL, related_name='invited_by')
    # Stats
    rides_taken = models.PositiveIntegerField(default=0)
    rides_offered = models.PositiveIntegerField(default=0)

    # status
    is_active = models.BooleanField('status activado', default=True, help_text='Los usuarios activos son los que interactuan con el circulo')


    def __str__(self):
        """ return username and circle"""
        return '@{} at #{}'.format(self.user.username, self.circle.slug_name)