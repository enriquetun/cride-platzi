"""Circle model."""

# Django

from django.db import models

# Utilities

from cride.utils.models import CRideModel

class Circle(CRideModel):
    """Circle model.
    
        Un circulo es un grupo donde es privado donde los raids. Para unirte a un circulo tienes que recibir una invitacion unica 
        de un miembro que ya es parte del grupo
    """
    name = models.CharField('nombre del circulo', max_length=140)
    slug_name = models.SlugField(unique=True, max_length=40)

    about = models.CharField('Descripcion del circulo', max_length=255)
    picture = models.ImageField(upload_to='circles/pictures', blank=True, null=True)
    # tabla miembros
    members = models.ManyToManyField('users.User', through='circles.Membership',  through_fields=('circle', 'user'))
    # Stats
    rides_offered = models.PositiveIntegerField(default=0)
    rides_taken = models.PositiveIntegerField(default=0)

    verified = models.BooleanField('Verificacion del circulo', default=False, help_text='Verifica circulos son tambien oficiales comunidades')

    is_public = models.BooleanField(default=True, help_text='Circulos publicos en la pagina principal si existe')
    is_limited = models.BooleanField('limite', default=False, help_text='Limite de circulos cuando el grupo de miembros ya se paso')
    membert = models.PositiveIntegerField(default=0, help_text='Si ek circulo es limitado, esta en su limite de numeros de miembros')


    def __str__(self):
        """Retorna el nombre de circulos"""
        return self.name
    
    class Meta(CRideModel.Meta):
        """Meta class."""
        # el menos es para indicar el orden de manera decendente
        ordering = ['-rides_taken', '-rides_offered']