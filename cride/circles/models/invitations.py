""" circulos invataciones modelo """

# Django
from django.db import models

# Utilities
from cride.utils.models import CRideModel

# Managers
from cride.circles.managers import InvitationManager

class Invitation(CRideModel):
    """ circulo invitacion 

        Una invitacion es un random de texto 
    
    """

    code = models.CharField(max_length=50, unique=True)

    issued_by = models.ForeignKey(
        'users.User',
        on_delete=models.CASCADE,
        help_text='Circulo miembro que es proveniente la invitacion',
        related_name='issued_by'
    )

    used_by = models.ForeignKey(
        'users.User',
        on_delete=models.CASCADE,
        null=True,
        help_text='Usuario usa este codigo para entrar al circulo',
    )
    circle = models.ForeignKey('circles.Circle', on_delete=models.CASCADE)

    used = models.BooleanField(default=False)
    used_at = models.DateTimeField(blank=True, null=True)

    # Manager
    objects = InvitationManager()

    def __str__(self):
        """ retorna codigo y circulo """ 
        return '#{}: {}'.format(self.circle.slug_name, self.code)