""" membership serializer """

 # Django REST Framework

from rest_framework import serializers

# Serializer
from cride.users.serializers import UserModelSerializer

# Model

from cride.circles.models import Membership, Invitation


class MembershipModelSerializer(serializers.ModelSerializer):
    """ model serializer membresias """

    user =  UserModelSerializer(read_only=True)
    joined_at = serializers.DateTimeField(source='created', read_only=True)
    invited_by = serializers.StringRelatedField()

    class Meta:
        model = Membership
        fields = (
            'user',
            'id_admin', 'is_active',
            'used_invitations', 'remaining_invitations',
            'invited_by',
            'rides_taken', 'rides_offered',
            'joined_at')
        read_only_fields = (
            'user',
            'used_invitations',
            'invited_by',
            'rides_taken', 'rides_offered'
        )



class AddMemberSerializer(serializers.Serializer):
    """ agrega miembro serializer.

        encargasrse de la añadir de un nuevo miembro para un circulo. 
        Circulo objeto debe ser proveniente en el contexto
    """

    invitation = serializers.CharField(min_length=8)
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())


    def validate_user(self, data):
        """ verifica usuario si ya es un miembro """ 

        circle =  self.context['circle']
        user = data

        q = Membership.objects.filter(circle=circle, user=user)

        if q.exists():
            raise serializers.ValidationError('Usuario es ya un miembro para el circulo')
    

    def validate_invitation_code(self, data):
        """ verifica codigo si existe y que esta relacionando por el circulo """ 

        try:
            invi = Invitation.objects.get(
                code=data,
                circle=self.context['circle'],
                used=False
            )
        except Invitation.DoesNotExist:
            raise serializers.ValidationError('cidogo de invitacion invalido')


        self.context['invitation'] = invi 

        return data