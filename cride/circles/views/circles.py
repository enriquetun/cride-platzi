""" Circles views. """

# Django REST Framework

from rest_framework import viewsets, mixins
from rest_framework.permissions import IsAuthenticated
# Serializer
from cride.circles.serializers import CircleModelSerializer
# Models
from cride.circles.models import Circle, Membership
# Permission
from cride.circles.permissions import IsCircleAdmin

class CircleViewSet(mixins.CreateModelMixin,
                   mixins.RetrieveModelMixin,
                   mixins.UpdateModelMixin,
                   mixins.ListModelMixin,
                   viewsets.GenericViewSet):
    """ Circulo view set es la vista de circulos """

    # queryset =  Circle.objects.all()
    serializer_class =  CircleModelSerializer
    lookup_field = 'slug_name'
    # permission_classes = (IsAuthenticated, )

    def get_queryset(self):
        queryset = Circle.objects.all()
        if self.action == 'list':
            return queryset.filter(is_public=True)
        return queryset
    
    def get_permissions(self):
        """Assign permissions based on action."""
        permissions = [IsAuthenticated]
        if self.action in ['update', 'partial_update']:
            permissions.append(IsCircleAdmin)
        return [permission() for permission in permissions]
    
    def perform_create(self, serializer):
        """ asignar circulo un administrador """
        circle = serializer.save()
        user = self.request.user
        profile = user.profile
        Membership.objects.create(
            user=user,
            profile=profile,
            circle=circle,
            id_admin=True,
            remaining_invitations=10
        )