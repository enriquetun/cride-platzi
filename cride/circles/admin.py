""" Circulos admin"""

# Django

from django.contrib import admin

# Utilities

from cride.circles.models import Circle, Membership, Invitation

@admin.register(Circle)
class CircleAdmin(admin.ModelAdmin):
    """Circle admin"""

    list_display = (
        'slug_name',
        'name',
        'is_public',
        'verified',
        'is_limited',
        'membert',
    )
    search_fields = ('slug_name', 'name')
    list_filter = (
        'is_public',
        'verified',
        'is_limited'
    )

@admin.register(Membership)
class MembershipAdmin(admin.ModelAdmin):
    pass

@admin.register(Invitation)
class InvitationAdmin(admin.ModelAdmin):
    pass
