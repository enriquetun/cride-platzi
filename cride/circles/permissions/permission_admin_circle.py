""" Permisos circulo class"""

# Django REST Framework
from rest_framework.permissions import BasePermission

# Models

from cride.circles.models import Membership


class IsCircleAdmin(BasePermission):
    """ Permitir acceso, en todo circulos permitidos """

    def has_object_permission(self, request, view, obj):
        
        try:
            Membership.objects.get(
                user=request.user,
                circle=obj,
                id_admin=True,
                is_active=True
            )
        except Membership.DoesNotExist:
            return False
        
        return True
    