""" permisos circulos por clases """

# Django REST Framework
from rest_framework.permissions import BasePermission 

# model
from cride.circles.models import Membership

class IsActiveCircleMember(BasePermission):
    """ accede los circulos que si tienen miembros 
    
    Execepcion las vistas que implementan los permisos tiene un 'circulo' como atributo asignado.
    """
    def has_object_permission(self, request, view):
        """ verifica usuario es uno activo miembro de el circulo."""
        
        try:
            Membership.objects.get(
                user=request.user,
                circle=view.circle,
                is_active=True
            )
        except Membership.DoesNotExist:
            return False
        return True