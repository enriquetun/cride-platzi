# Generated by Django 2.0.9 on 2020-04-27 01:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('circles', '0002_auto_20200204_0502'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='circle',
            options={'get_latest_by': 'created', 'ordering': ['-rides_taken', '-rides_offered']},
        ),
        migrations.AlterField(
            model_name='membership',
            name='id_admin',
            field=models.BooleanField(default=False, help_text='Circulos administradores pueden actualizar o borrar datos de los miembros', verbose_name='circle admin'),
        ),
    ]
