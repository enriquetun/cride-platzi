"""Perfil model."""


#Django
from django.db import models

#Utilities
from cride.utils.models import CRideModel


class Profile(CRideModel):
    """Perfil model.
    Un perfil sostiene la publicacion de la bibliografia, imagenes y estadisticas"""

    users = models.OneToOneField('users.User', on_delete=models.CASCADE)
    picture = models.ImageField('perfil imagen', upload_to='users/pictures', blank=True, null=True)
    biography = models.TextField(max_length=500, blank=True)
    #stadisticas(Stats)
    rides_taken = models.PositiveIntegerField(default=0)
    rides_offered = models.PositiveIntegerField(default=0)
    reputation =  models.FloatField(default=5.0, help_text="Reputacion de usuarios en los paseos tomados o ofrecidos")

    def __str__(self):
        """return user str representation"""
        return str(self.users)