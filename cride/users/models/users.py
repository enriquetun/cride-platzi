"""User model."""

"""Django"""
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.core.validators import RegexValidator



"""User model."""

# Django 
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.core.validators import RegexValidator
# Utilities (Utilidades)
from cride.utils.models import CRideModel

class User(CRideModel, AbstractUser):

    """Este modelo 
    sirve para extender datos de los usuarios y email e otros campos en pocas palabras agregar mas campos al registro"""
    email = models.EmailField('email_address', unique=True, error_messages={
        'unique': 'Usuario con este correo existe.'
    })

    phone_regex = RegexValidator(regex=r'\+?1?\d{9,15}$', message="Numero telefonico debe ser entero en el formato: +999999999. Puedes usar 15 digitos maximo.")

    phone_number = models.CharField(validators=[phone_regex], max_length=17, blank=True)
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username', 'first_name', 'last_name']

    is_client = models.BooleanField(
        'client_status', default=True, help_text=(
            'Ayuda facilmente al usaurio a disitinguir y  realizar consultas.'
            'Los clientes son los principales typo de usuarios'
        ) 
    )
    is_verificado = models.BooleanField('verified', default=True, help_text='Establece verdadero si el usuario tiene verificado su email')

    def __str__(self):
        """return username"""
        return self.username
    
    def get_short_name(self):
        """Return username."""
        return self.username