"""User models admin."""

#Django
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

# Model
from cride.users.models import User, Profile


class CustomUserAdmin(UserAdmin):
    """user model admin."""
    list_display = ('email', 'username', 'last_name', 'first_name', 'is_staff', 'is_client')
    list_filter = ('is_client', 'is_staff', 'created', 'modified')



@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    """Profile model admin."""
    list_display = ('users', 'reputation', 'rides_taken', 'rides_offered')
    search_fields = ('users__username', 'users__email', 'users__last_name')
    list_filter = ('reputation',)



admin.site.register(User ,CustomUserAdmin)