""" user views."""

# Django REST FRAMEWORK
from rest_framework import status, viewsets, mixins
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.views import APIView

# Permisos
from rest_framework.permissions import AllowAny, IsAuthenticated
from cride.users.permissions import IsAccountOwner

#Serializers
from cride.users.serializers import UserLoginSerializer, UserModelSerializer, UserSigUpSerializer, AccountVerificationSerializer
from cride.circles.serializers import CircleModelSerializer
from cride.users.serializers.profile import ProfileModelSerializer

# models
from cride.users.models import User
from cride.circles.models import Circle

class UserViewSet(mixins.RetrieveModelMixin,
                  mixins.UpdateModelMixin,
                 viewsets.GenericViewSet):
    """ Usuario view set.

    Escoge crear usuario, login y token de verificacion"""

    queryset = User.objects.filter(is_active=True, is_client=True)
    serializer_class = UserModelSerializer
    lookup_field = 'username'

    def get_permissions(self): 
        """Asigna permisos para un usario en particular"""
        if self.action in ['signup', 'login', 'verify']:
            permissions = [AllowAny]
        elif self.action in ['retrieve', 'update', 'partial_update' ]:
            permissions = [IsAuthenticated, IsAccountOwner]
        else:
            permissions = [IsAuthenticated]
        return [p() for p in permissions]


    @action(detail=False, methods=['post'])
    def login(self, request):
        """ Login de un usuario"""
        serializer = UserLoginSerializer(data = request.data)
        serializer.is_valid(raise_exception=True)
        user, token = serializer.save()
        data = {
            'user': UserModelSerializer(user).data,
            'access_token': token
        }
        return Response(data, status=status.HTTP_201_CREATED)

    @action(detail=False, methods=['post'])
    def signup(self, request):
        """ crear un usuario nuevo """
        serializer = UserSigUpSerializer(data = request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.save()
        data  = UserModelSerializer(user).data
        return Response(data, status=status.HTTP_201_CREATED)

    @action(detail=False, methods=['post'])
    def verify(self, request):
        """ verificacion de la cuenta"""
        serializer = AccountVerificationSerializer(data = request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        data = {'message': 'Validacion exitosa'}
        return Response(data, status=status.HTTP_200_OK)

    @action(detail=True, methods=['put', 'patch'])
    def profile(self, request, *args, **kwargs):
        """ Actualizacion profile datos."""
        user =  self.get_object()
        profile = user.profile
        partial = request.method == 'PATCH'
        serializer = ProfileModelSerializer(
            profile,
            data=request.data,
            partial=partial
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()
        data = UserModelSerializer(user).data
        return Response(data)


    def retrieve(self, request, *args, **kwargs):
        """ Agregar extra data ala respuesta """
        response =  super(UserViewSet, self).retrieve(request, *args, **kwargs)
        circles = Circle.objects.filter(
            members=request.user,
            membership__is_active=True
        )
        data = {
            'user': response.data,
            'circles': CircleModelSerializer(circles, many=True).data
        }
        response.data = data
        return response




""" Este codigo de abajo con clases basadas """

# class UserLoginAPIVIew(APIView):
#     """User login API view."""
#     def post(self, request, *args, **kwargs):
#         """Encargarse HTTP POST reques."""
#         serializer = UserLoginSerializer(data = request.data)
#         serializer.is_valid(raise_exception=True)
#         user, token = serializer.save()
#         data = {
#             'user': UserModelSerializer(user).data,
#             'access_token': token
#         }
#         return Response(data, status=status.HTTP_201_CREATED)


# class UserSigUpAPIView(APIView):
#     """User Sig Up API view. Sirve para crear"""
#     def post(self, request, *args, **kwargs):
#         """Encargarse HTTP POST reques."""
#         serializer = UserSigUpSerializer(data = request.data)
#         serializer.is_valid(raise_exception=True)
#         user = serializer.save()
#         data  = UserModelSerializer(user).data
#         return Response(data, status=status.HTTP_201_CREATED)



# class AccountVerificationAPIView(APIView):
#     """ User verification API view. """
#     def post(self, request, *args, **kwargs):
#         """Encargarse HTTP POST reques."""
#         serializer = AccountVerificationSerializer(data = request.data)
#         serializer.is_valid(raise_exception=True)
#         serializer.save()
#         data = {'message': 'Validacion exitosa'}
#         return Response(data, status=status.HTTP_200_OK)

