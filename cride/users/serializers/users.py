"""User serializers"""
#Django
from django.conf import settings
from django.contrib.auth import password_validation, authenticate
from django.core.validators import RegexValidator
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils import timezone
#Django REST FRAMEWORK

from rest_framework import serializers
from rest_framework.authtoken.models import Token
from rest_framework.validators import UniqueValidator

# Models
from cride.users.models import User, Profile

# Serializer
from cride.users.serializers.profile import ProfileModelSerializer


# Utilities 
import jwt
from datetime import timedelta

class UserModelSerializer(serializers.ModelSerializer):
    """Modelo usuario serializer"""
    profile = ProfileModelSerializer(read_only=True)
    class Meta:
        """Meta class."""
        model = User
        fields = (
            'username', 'first_name', 'last_name', 'email', 'phone_number', 'profile'
        )

class UserSigUpSerializer(serializers.Serializer):
    """Usuario signup serializer
    
    Se encarga de subir data validarlo y usuario/perfil crearlo.
    """
    email = serializers.EmailField(
        validators=[UniqueValidator(queryset=User.objects.all())]
    )
    username = serializers.CharField(
        min_length = 4,
        max_length = 20,
        validators=[UniqueValidator(queryset=User.objects.all())]
    )
    #Phone number
    phone_regex = RegexValidator(
        regex=r'\+?1?\d{9,15}$', 
        message="Numero telefonico debe ser entero en el formato: +999999999. Puedes usar 15 digitos maximo."
    )
    phone_number =  serializers.CharField(validators=[phone_regex])
    # Password
    password = serializers.CharField(min_length=8, max_length=64)
    password_confirmation =  serializers.CharField(min_length=8, max_length=64)
    # Name
    first_name = serializers.CharField(min_length=2, max_length=64)
    last_name = serializers.CharField(min_length=2, max_length=64)

    def validate(self, data):
        """Verifica passwords si hacen match."""
        passwd = data['password']
        passwd_conf =  data['password_confirmation']

        if passwd != passwd_conf:
            raise serializers.ValidationError("Password no hizo match.")
        password_validation.validate_password(passwd)
        return data

    def create (self, data):
        """Varicar usuario y perfil creacion."""
        data.pop('password_confirmation')
        user = User.objects.create_user(**data, is_verificado=False, is_client=True)
        profile = Profile.objects.create(users=user)
        self.send_confirmation_email(user)
        return user


    def send_confirmation_email(self, user):
        """ Envio del email para la verificacion de link del usuario"""
        verification_token = self.gen_verication_token(user)
        subject = 'Bienvenido @{}! Verifica tu cuenta para iniciar el uso de Compart Ride'.format(user.username)
        from_email = 'Compart Ride <noreply@comparteride.com>'
        content = render_to_string('emails/users/account_verification.html', {
            'token': verification_token,
            'user': user
        })
        msg = EmailMultiAlternatives(subject, content, from_email, [user.email])
        msg.attach_alternative(content, "text/html")
        msg.send()
    
    def gen_verication_token(self, user):
        """ vericacion del token de autentificacion"""
        exp_date = timezone.now() + timedelta(days=4)
        payload = {
            'user': user.username,
            'exp': int(exp_date.timestamp()),
            'type': 'email_confirmation'
        }
        token = jwt.encode(payload, settings.SECRET_KEY, algorithm='HS256')
        return token.decode()

class UserLoginSerializer(serializers.Serializer):
    """Usuario login serializer.
    Se encarga el login request data"""

    email  = serializers.EmailField()
    password = serializers.CharField(min_length=8)

    def validate(self, data):
        """checa credenciales si son validas. Usa el metodo authenticate para hacer el login"""
        user = authenticate(username=data['email'], password=data['password'])

        if not user:
            raise serializers.ValidationError('Credenciales invalidas')
        if not user.is_verificado:
            raise serializers.ValidationError('Cuenta no validada :(')
        self.context['user'] = user
        return data

    def create(self, data):
        """Genera o recupera un nuevo token."""
        token, created = Token.objects.get_or_create(user=self.context['user'])
        return self.context['user'], token.key



class AccountVerificationSerializer(serializers.Serializer):
    """ Serializer para validar el token de entrada  y hacer valido la cuenta"""

    token = serializers.CharField()

    def validate_token(self, data):
        """ Vericacion del token de entrada jwt ojo el PyJWTError es el arbol de exception de la documentacion""" 
        try:
            payload = jwt.decode(data, settings.SECRET_KEY, algorithm=['HS256'])
        except jwt.ExpiredSignatureError:
            raise serializers.ValidationError('El link a expirado')
        except jwt.PyJWTError:
            raise serializers.ValidationError('Token invalido')


        if payload['type'] != 'email_confirmation':
            raise serializers.ValidationError('Token invalido')

        
        self.context['payload'] = payload
        return data
    
    def save(self):
        """Actualiza el estado de verificacion """
        payload = self.context['payload']
        user = User.objects.get(username=payload['user'])
        user.is_verificado = True
        user.save()