""" Permisos para usuarios """

# Django REST Framework
from rest_framework.permissions import BasePermission

class IsAccountOwner(BasePermission):
    """ Permiso para que el usario solo pueda acceder al detalle de el mismo """
    def has_object_permission(self, request, view, obj):
        """ Checa obj y el usuario para saber si son ellos mismos"""
        return request.user == obj